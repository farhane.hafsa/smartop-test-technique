const Intervention = require('../models/Intervention');
const PAGE_SIZE = 10;

//app controller

// This function return a list of surgeons classed by the number of interventions
exports.getSurgeonsSortByNumberOfInterventions = async (req, res, next) => {
    const pageIndex = req.query.page;
    const surgeonName = req.query.name || '';
    const count = await Intervention.distinct('surgeon');
    const surgeons = await Intervention.aggregate([
        //We use the regexMatch to search for a surgeon bu its name
        // ERR: $regexMatch is not allowed in this atlas tier
        // {$regexMatch: {input: '$surgeon', regex: '/.*' + surgeonName + '/.*'}},
        {
            $group: {
                _id: "$surgeon",
                count: {$sum: 1},
                speciality: {$first: "$specialty"},
                anesthsistes: {
                    $push: {
                        "$cond": [
                            {"$eq": ["$anesthsiste", '']},
                            "$noval",
                            '$anesthsiste'
                        ]
                    }
                },
                nurse1: {
                    $push: '$nurse1',
                },
                nurse2: {
                    $push: '$nurse2',
                },
                rooms: {
                    $push: '$roomNumber'
                },
                intervention: {
                    $push: '$intervention'
                }
            },

        },
        {$sort: {count: -1}},
        {$skip: PAGE_SIZE * pageIndex},
        {$limit: PAGE_SIZE}
    ]);
    // organize the result
    for (let surgeon of surgeons) {

        //rename attribut _id
        surgeon['name'] = surgeon['_id'];
        delete surgeon['_id'];

        //Calculate the room when the suregon has most worked
        surgeon['favoriteRoom'] = getFavoriteRoom(surgeon);
        delete surgeon['rooms'];

        //Calculate the nurse that the surgeon has worked with the most
        surgeon['favoriteNurse'] = getFavoriteNurse(surgeon);
        delete surgeon['nurse1'];
        delete surgeon['nurse2'];

        //Calculate the anesthsiste that the surgeon has worked with the most
        surgeon['favoriteAnesthsiste'] = getFavoriteAnesthsiste(surgeon);
        delete surgeon['anesthsistes'];

        //Calculate the most frequente intervention
        surgeon['favoriteIntervention'] = getFavoriteIntervention(surgeon);
        delete surgeon['intervention'];

    }

    //return a list of surgeons
    res.status(200).json({
        count: count.length,
        content: surgeons
    });
}

//Calculate the room when the surgeon has most worked, it takes surgeon as parameter and return the favorite room
function getFavoriteRoom(surgeon) {
    let rooms = {};
    for (const room of surgeon.rooms) {
        if (rooms[room]) {
            rooms[room]++;
        } else {
            rooms[room] = 1
        }
    }
    return Object.keys(rooms).reduce((a, b) => rooms[a] > rooms[b] ? a : b);
}

//Calculate the nurse whom the surgeon has most worked with, it takes surgeon as parameter and return the favorite nurse
function getFavoriteNurse(surgeon) {
    const listOfNurses = surgeon.nurse1.concat(surgeon.nurse2);
    let nurses = {};
    for (let nurse of listOfNurses) {
        if (nurses[nurse]) {
            nurses[nurse]++;
        } else {
            nurses[nurse] = 1;
        }
    }
    return Object.keys(nurses).reduce((a, b) => nurses[a] > nurses[b] ? a : b);
}

//Calculate the anesthsiste whom the surgeon has most worked with, it takes surgeon as parameter and return the favorite anesthsiste
function getFavoriteAnesthsiste(surgeon) {
    let anesthsistes = {};
    for (let anesthsiste of surgeon.anesthsistes) {
        if (anesthsistes[anesthsiste]) {
            anesthsistes[anesthsiste]++;
        } else {
            anesthsistes[anesthsiste] = 1
        }
    }
    if (Object.keys(anesthsistes).length > 0) {
        return Object.keys(anesthsistes).reduce((a, b) => anesthsistes[a] > anesthsistes[b] ? a : b);
    } else {
        return null;
    }

}


//Calculate the intervention that the surgeon has mostly done, it takes surgeon as parameter and return the intervention
function getFavoriteIntervention(surgeon) {
    let interventions = {};
    for (let intervention of surgeon.intervention) {
        if (interventions[intervention]) {
            interventions[intervention]++;
        } else {
            interventions[intervention] = 1
        }
    }
    return Object.keys(interventions).reduce((a, b) => interventions[a] > interventions[b] ? a : b);
}
