const express = require('express');
const router = express.Router();
const interventionController = require('../controllers/InterventionController');

// app routes
router.get('/surgeons', interventionController.getSurgeonsSortByNumberOfInterventions);

module.exports = router;
