//This file represent the schema intervention
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const InterventionSchema = new Schema({
    id: {type: Schema.Types.ObjectId},
    surgeon: {type: String},
    specialty: {type: String},
    anesthsiste: {type: String},
    nurse1: {type: String},
    nurse2: {type: String},
    roomNumber: {type: Number},
    intervention: {type: String},
});

module.exports = mongoose.model('intervention', InterventionSchema);
