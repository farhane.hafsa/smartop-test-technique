const express = require('express');
const mongoose = require('mongoose');
const app = express();
const interventionRoutes = require('./src/routes/IntervetionsRoutes');

// Connection with MongoDB USERNAME : SmartOp_user , PASSWORD : SmartOp_2020 , DATABASE_NAME : SmartOp_DB
mongoose.connect(
    'mongodb+srv://SmartOp_user:SmartOp_2020@cluster0.dyf7t.mongodb.net/SmartOp_DB?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(express.json());
app.use('/', interventionRoutes);

module.exports = app;
