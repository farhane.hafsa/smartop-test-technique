import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
// here we communicate with the backend to get all surgeons from the repository

  constructor(private http: HttpClient) {

  }

  public findAllSurgeons(): Observable<any> {
    return this.http.get('');
  }

  public fetchSurgeons(page: number, name: string): Observable<any> {
    return this.http.get('http://localhost:3000/surgeons', {
      params: {
        page: '' + page,
        name
      }
    });
  }
}
