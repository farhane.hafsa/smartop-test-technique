import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {AppService} from './app.service';
import {debounceTime, map, startWith, switchMap} from 'rxjs/operators';
import {FormControl, FormGroup} from '@angular/forms';
import {combineLatest, Subscription} from 'rxjs';

export interface Surgeon {
  name: string;
  speciality: string;
  count: number;
  favoriteAnesthsiste: string;
  favoriteNurse: string;
  favoriteRoom: number;
  favoriteIntervention: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  AfterViewInit, OnDestroy {

  displayedColumns: string[] = [
    'name',
    'speciality',
    'count',
    'favoriteAnesthsiste',
    'favoriteNurse',
    'favoriteRoom',
    'favoriteIntervention'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  resultsLength: number;
  dataSource: MatTableDataSource<Surgeon> = new MatTableDataSource<Surgeon>();
  searchFormGroup: FormGroup = new FormGroup({
    name: new FormControl()
  });
  name: string;
  subscription: Subscription;

  constructor(
    private appService: AppService
  ) {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.subscription = combineLatest(
      [
        this.paginator.page,
        this.searchFormGroup.valueChanges.pipe(
          debounceTime(300)
        )
      ]
    ).pipe(
      startWith([
        {
          pageIndex: 0,
          pageSize: 10
        },
        {
          name: ''
        }]
      ),
      switchMap(([page, form]) => {
        // @ts-ignore
        return this.appService.fetchSurgeons(page.pageIndex, form.name);
      }),
      map((response) => {
        this.resultsLength = response.count;
        return response.content;
      })
    ).subscribe((data) => {
      this.dataSource.data = data;
    });
    this.paginator.page.emit({
      pageIndex: 0,
      pageSize: 10,
      previousPageIndex: 0,
      length: 0
    });
    this.searchFormGroup.setValue({name: ''});
  }

}
