# SmartOp Test technique : Classement des chirurgiens par intervention

## Lancer le frontend
```bash
$ cd SmartOp-Frontend
$ npm install
$ npm run start
```

## Lancer le backend
```bash
$ cd SmartOp-Backend
$ npm run start
```

## Informations sur la base de données

J'ai utilisé une base de données MongoDB sur cloud : **MongoDB Atlas**

| Nom de la base de données | nom d'utilisateur | Mot de passe |
| ------------------------  | ----------------- | ------------ |
| SmartOp_DB                | SmartOp_user      | SmartOp_2020 |

## Suivi du Cahier des charges

|           *Fonctionnalités* attendues                         |   Implémentation    |                                                              |
| :-----------------------------------------------------------: | :-----------------: | ------------------------------------------------------------ |
| Un tableau de classement des chirurgiens                      | *Implémenté (100%)* |                                                              |
| La pagination pour n’afficher que 10 profils à la fois.       | *Implémenté (100%)* |                                                              |
| Le scroll chargera automatiquement les 10 profils suivants    | *Implémenté (100%)* |                                                              |
| Une barre de recherche par nom de chirurgien                  | *Implémenté (80%)* | Comme j'ai utilisé une base de données sur cloud la commande pour effectuer la recherche par nom n'a pas pu être exécuter, il me faut une version payante de MangoDB atlas pour la faire |

## Auteur
Hafsa FARHANE LAACHIRI
